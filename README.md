# Projet Ansible

Mostefa ALI KADA  
Yann MERLIN

## Adresses IP

VM: Ce projet necessite deux serveurs et un node manager.

Il faut modifier les fichiers /inventories/hosts.yml, /files/site/index_prod.php et /files/site/index_staging.php. 

Dans /inventories/hosts.yml :
```
all:
  hosts:
    server1: #Production
      ansible_host: METTRE ICI L'@IP DE LA VM DE PRODUCTION
      ansible_user: debian

    server2: #Staging
      ansible_host: METTRE ICI L'@IP DE LA VM DE STAGING
      ansible_user: debian
```

Dans /files/site/index_prod.php :
```
$servername = METTRE ICI L'@IP DE LA VM DE PRODUCTION;
```

Dans /files/site/index_staging.php :
```
$servername = METTRE ICI L'@IP DE LA VM DE STAGING;
```

## Commandes

Les commandes à éxecuter pour le déploiement sont les suivantes :


ansible-playbook install-mariadb_prod.yml -i inventories

ansible-playbook install-mariadb_staging.yml -i inventories

ansible-playbook install-nginx.yml -i inventories

ansible-playbook install-php.yml -i inventories

ansible-playbook install-site_prod.yml -i inventories

ansible-playbook install-site_staging.yml -i inventories

## Tests

Pour tester le fonctionnement des environnement, sur un navigateur entrez les URLs :

```
http://{METTRE ICI L'@IP DE LA VM DE PRODUCTION}/index_prod.php
```
```
http://{METTRE ICI L'@IP DE LA VM DE STAGING}/index_staging.php
```

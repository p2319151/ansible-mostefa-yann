<?php

echo "Test de la connexion en prod : ";

$servername = "10.42.191.215";
$username = "user_prod";
$password = "pwd_prod";
$database = "db_prod";

// Créer une connexion
$conn = new mysqli($servername, $username, $password, $database);

// Vérifier la connexion
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
echo "Connected successfully";

// Fermer la connexion
$conn->close();

?>
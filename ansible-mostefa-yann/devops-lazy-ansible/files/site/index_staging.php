<?php

echo "Test de la connexion en staging : ";

$servername = "10.42.184.236";
$username = "user_staging";
$password = "pwd_staging";
$database = "db_staging";

// Créer une connexion
$conn = new mysqli($servername, $username, $password, $database);

// Vérifier la connexion
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
echo "Connected successfully";

// Fermer la connexion
$conn->close();

?>